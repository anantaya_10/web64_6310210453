
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function AboutUs (props){


    return (
        <Box sx={{ width:"60%" }} >
            <Paper elevation={3}>
                <h2> Enter Name: { props.name } </h2>
                <h3> Enter Surname: { props.surname } </h3>
                <h3> Enter ID: { props.number } </h3>
            </Paper>
        </Box>
    );
}

export default AboutUs;
