import logo from './logo.svg';
import './App.css';

import AboutHomePage from './pages/AboutHomePage';
import patronPage from './pages/patronPage';
import Header from './components/Header';

import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
         <Header />
         <Routes>
             <Route path="about" element={
                      <AboutHomePage />
                     } /> 

             <Route path="/" element={
                       <patronPage />
                     } />

        </Routes>
    </div>
  );
}

export default App;
